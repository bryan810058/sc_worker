package com.slivercare.scworker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MainActivity extends Activity {

    TextView isbeacon, status;
    ImageView beacon;

    private static final String LOG_TAG = "MainActivity";
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private Handler scanHandler = new Handler();
    private int scan_interval_ms = 10000;
    private boolean isScanning = false;

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        isbeacon = (TextView) findViewById(R.id.isbeacon);
        beacon = (ImageView) findViewById(R.id.imageView6);
        status = (TextView) findViewById(R.id.status);

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        // Hashmap to load data from the Sqlite database
        HashMap<String, String> task = new HashMap<String, String>();
        HashMap<String, String> check = new HashMap<String, String>();
        task = db.getTaskDetails();

        if (task.get("id") != null) {
            status.setText("尚未到達指定地點");
            if (check.get("check_test") == null) {
                status.setText("尚未到達指定地點");
            }else{
                status.setText("到達指定地點，已更新任務狀況");
            }

        } else {
            status.setText("尚未接受任務，點選下方Task按鈕瀏覽任務清單");
        }

        // init BLE
        btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        if (!btAdapter.isEnabled())
            btAdapter.enable();

        scanHandler.post(scanRunnable);


//        tv.setText(get);


    }


    private Runnable scanRunnable = new Runnable() {
        @Override
        public void run() {
            if (isScanning) {
                if (btAdapter != null) {
                    btAdapter.stopLeScan(leScanCallback);
                }
            } else {
                if (btAdapter != null) {
                    btAdapter.startLeScan(leScanCallback);
                }
            }
            isScanning = !isScanning;
            scanHandler.postDelayed(this, scan_interval_ms);
        }
    };


    public BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            int startByte = 2;
            boolean patternFound = false;
            while (startByte <= 5) {
                if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                    patternFound = true;
                    break;
                }
                startByte++;
            }
            if (patternFound) {
                //Convert to hex String
                byte[] uuidBytes = new byte[16];
                System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                String hexString = bytesToHex(uuidBytes);
                //UUID detection
                String uuid = hexString.substring(0, 8) + "-" +
                        hexString.substring(8, 12) + "-" +
                        hexString.substring(12, 16) + "-" +
                        hexString.substring(16, 20) + "-" +
                        hexString.substring(20, 32);
                // major
                final int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);
                // minor
                final int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);


                // Hashmap to load data from the Sqlite database
                HashMap<String, String> task = new HashMap<String, String>();
                HashMap<String, String> check = new HashMap<String, String>();
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                task = db.getTaskDetails();
                check = db.getCheck();

                // 測試用
                if (check.get("check_test") == null) {
                    Log.d("check", "null");
                } else {
                    Log.d("check", check.get("check_test"));
                }

                beacon.setVisibility(View.INVISIBLE);
                isbeacon.setVisibility(View.INVISIBLE);

                if (task.get("id") != null) {
                    beacon.setVisibility(View.VISIBLE);
                    isbeacon.setVisibility(View.VISIBLE);
                    status.setText("已接受任務，請盡速到達指定地點");
                    if (check.get("check_test") == null) {
                        if (Integer.valueOf(task.get("major")) == major && Integer.valueOf(task.get("minor")) == minor) {
                            db.addCheck("1");
                            new WorkerStartTime().execute();

                        } else {
                            status.setText("尚未到達指定地點");
                        }
                    } else {
                        status.setText("到達指定地點，已更新任務狀況");
                    }

                } else {
                    status.setText("尚未接受任務，點選下方Task按鈕瀏覽任務清單");
                }


                Log.i(LOG_TAG, "UUID: " + uuid + "\\nmajor: " + major + "\\nminor" + minor);
            }

        }
    };

    /**
     * bytesToHex method
     */
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    class WorkerStartTime extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("到達指定地點，上傳任務開始時間...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.TAIWAN);
            String date = sDateFormat.format(new java.util.Date());

            Log.d("TIME", date);

            // Hashmap to load data from the Sqlite database
            HashMap<String, String> task = new HashMap<String, String>();
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            task = db.getTaskDetails();

            //只需上傳 Worker id 、 get_time
            // Building Parameters
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", task.get("id")));
            params.add(new BasicNameValuePair("start_time", date));

            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(Url.url_worker_start_time, "POST", params);

            // check json success tag
            try {
                int success = json.getInt("success");

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();

        }
    }


    public void goto_task(View v) {

        Intent it = new Intent(this, Task.class);
        startActivity(it);

    }

    public void goto_process(View v) {

        Intent it = new Intent(this, Process.class);
        startActivity(it);

    }

    public void goto_history(View v) {

        Intent it = new Intent(this, History.class);
        startActivity(it);

    }
}
