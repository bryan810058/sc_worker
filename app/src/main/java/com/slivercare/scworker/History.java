package com.slivercare.scworker;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by bryan on 2016/8/28.
 */
public class History extends ListActivity {


    // Progress Dialog
    private ProgressDialog pDialog;
    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();
    ArrayList<HashMap<String, String>> productsList;
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "products";
    private static final String TAG_ID = "id";
    private static final String TAG_USER_ID = "user_id";
    private static final String TAG_TYPE = "type";
    private static final String TAG_TIME = "time";
    private static final String TAG_GET_TIME = "get_time";
    private static final String TAG_FINISH_TIME = "finish_time";
    private static final String TAG_PROCESS = "process";
    private static final String TAG_SERVICE_ID = "service_id";
    private static final String TAG_NOTE = "note";

    String id,user_id,type,time,get_time,finish_time,process,service_id,note;

    TextView history;
    // products JSONArray
    JSONArray products = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        // Hashmap for ListView
        productsList = new ArrayList<HashMap<String, String>>();

        history = (TextView)findViewById(R.id.history);
        //為了加底線
        SpannableString content = new SpannableString("我的歷史任務清單");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        history.setText(content);

        // Get listview
        ListView lv = getListView();

        new WorkerHistory().execute();
    }


    class WorkerHistory extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(History.this);
            pDialog.setMessage("取得任務詳細資料...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            // updating UI from Background Thread

            // Check for success tag
            int success;
            try {
                // Building Parameters
                List<NameValuePair> gparams = new ArrayList<>();

//                        gparams.add(new BasicNameValuePair("pid", pid));
                gparams.add(new BasicNameValuePair("worker_id", "1"));

                // getting product details by making HTTP request
                // Note that product details url will use GET request
                JSONObject json = jsonParser.makeHttpRequest(Url.url_worker_get_history, "GET", gparams);

                // check your log for json response
                Log.d("Worker History", json.toString());

                // json success tag
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    // successfully received product details
                    products = json.getJSONArray(TAG_PRODUCTS);

                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        // product with this pid found
                        // Edit Text

                        // display product data in EditText

                        id = c.getString(TAG_ID);
                        user_id = c.getString(TAG_USER_ID);
                        type = c.getString(TAG_TYPE);
                        time = c.getString(TAG_TIME);
                        get_time = c.getString(TAG_GET_TIME);
                        finish_time = c.getString(TAG_FINISH_TIME);
                        process = c.getString(TAG_PROCESS);
                        service_id = c.getString(TAG_SERVICE_ID);
                        note = c.getString(TAG_NOTE);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();
                        // adding each child node to HashMap key => value

                        if(Integer.valueOf(type)==1)
                            type = "緊急聯絡";
                        else if(Integer.valueOf(type)==2)
                            type = "盥洗";
                        else if(Integer.valueOf(type)==3)
                            type = "外出";
                        else if(Integer.valueOf(type)==4)
                            type = "餐飲";
                        else if(Integer.valueOf(type)==5)
                            type = "清潔";

                        map.put(TAG_ID, String.valueOf(i));
                        map.put(TAG_TYPE, type);
                        map.put(TAG_FINISH_TIME, finish_time);
                        map.put(TAG_SERVICE_ID, service_id);

                        // adding HashList to ArrayList
                        productsList.add(map);

                    }
                } else {
                    // product with pid not found
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once got all details
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(History.this, productsList,
                            R.layout.task_items, new String[]{TAG_ID, TAG_TYPE, TAG_SERVICE_ID, TAG_FINISH_TIME},
                            new int[]{R.id.task_items_id, R.id.task_items_type, R.id.task_items_service_id, R.id.task_items_note});

                    setListAdapter(adapter);
                }
            });


        }
    }
}
