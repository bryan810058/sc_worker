package com.slivercare.scworker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.slivercare.scworker.DatabaseHandler;
import com.slivercare.scworker.TaskFunctions;

/**
 * Created by bryan on 2016/8/23.
 */
public class Receive_task extends Activity {

    TextView receive_name, receive_address, receive_age, receive_sex, receive_notice;
    TextView receive_id, receive_type, receive_time, receive_service_id, receive_note;

    TextView task_info;

    Button btnReceive;

    String pid;

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // single product url
    private static final String url_get = Url.url_get_task_detail;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCT = "product";

    private static final String TAG_ID = "id";

    String name, address, age, sex, notice;
    String id, type, time, service_id, note;
    String major, minor;

    String check;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receive_task);


        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        // Hashmap to load data from the Sqlite database
        HashMap<String, String> task = new HashMap<String, String>();
        task = db.getTaskDetails();

        check = task.get("id");

        // save button
        btnReceive = (Button) findViewById(R.id.receive_button);

        receive_name = (TextView) findViewById(R.id.receive_name);
        receive_address = (TextView) findViewById(R.id.receive_address);
        receive_age = (TextView) findViewById(R.id.receive_age);
        receive_sex = (TextView) findViewById(R.id.receive_sex);
        receive_notice = (TextView) findViewById(R.id.receive_notice);

        receive_id = (TextView) findViewById(R.id.receive_id);
        receive_type = (TextView) findViewById(R.id.receive_type);
        receive_time = (TextView) findViewById(R.id.receive_time);
        receive_service_id = (TextView) findViewById(R.id.receive_service_id);
        receive_note = (TextView) findViewById(R.id.receive_note);

        task_info = (TextView) findViewById(R.id.task_info);

        //為了加底線
        SpannableString content = new SpannableString("任務詳細資料");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        task_info.setText(content);


        // getting product details from intent
        Intent i = getIntent();

        // getting product id (pid) from intent
        pid = i.getStringExtra(TAG_ID);

        // Getting complete product details in background thread
        new GetProductDetails().execute();

        // save button click event
        btnReceive.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String id = receive_id.getText().toString();
                String type = receive_type.getText().toString();
                String time = receive_time.getText().toString();
                String service_id = receive_service_id.getText().toString();
                String note = receive_note.getText().toString();

                // 檢查是否已有領取的任務
                if (check == null) {
                    new ReceiveTask().execute(id, type, time, service_id, note);
                } else {
                    Context context = getApplication();
                    Toast.makeText(context, "已有任務進行中!", Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    class GetProductDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Receive_task.this);
            pDialog.setMessage("取得任務詳細資料...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            // updating UI from Background Thread

            // Check for success tag
            int success;
            try {
                // Building Parameters
                List<NameValuePair> gparams = new ArrayList<>();

//                        gparams.add(new BasicNameValuePair("pid", pid));
                gparams.add(new BasicNameValuePair("id", pid));

                // getting product details by making HTTP request
                // Note that product details url will use GET request
                JSONObject json = jsonParser.makeHttpRequest(url_get, "GET", gparams);

                // check your log for json response
                Log.d("Single Product Details", json.toString());

                // json success tag
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    // successfully received product details
                    JSONArray productObj = json.getJSONArray(TAG_PRODUCT); // JSON Array

                    // get first product object from JSON Array
                    JSONObject product = productObj.getJSONObject(0);

                    // product with this pid found
                    // Edit Text

                    // display product data in EditText

                    name = product.getString("name");
                    address = product.getString("address");
                    age = product.getString("age");
                    sex = product.getString("sex");
                    notice = product.getString("notice");

                    id = product.getString("id");
                    type = product.getString("type");
                    time = product.getString("time");
                    service_id = product.getString("service_id");
                    note = product.getString("note");

                    major = product.getString("major");
                    minor = product.getString("minor");


                } else {
                    // product with pid not found
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once got all details

            if(Integer.valueOf(sex) == 1){
                sex = "男";
            }
            else{
                sex = "女";
            }

            if (Integer.valueOf(type) == 1)
                type = "緊急聯絡";
            else if (Integer.valueOf(type) == 2)
                type = "盥洗";
            else if (Integer.valueOf(type) == 3)
                type = "外出";
            else if (Integer.valueOf(type) == 4)
                type = "餐飲";
            else if (Integer.valueOf(type) == 5)
                type = "清潔";

            receive_name.setText(name);
            receive_address.setText(address);
            receive_age.setText(age);
            receive_sex.setText(sex);
            receive_notice.setText(notice);

            receive_id.setText(id);
            receive_type.setText(type);
            receive_time.setText(time);
            receive_service_id.setText(service_id);
            receive_note.setText(note);

            pDialog.dismiss();
        }
    }

    class ReceiveTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Receive_task.this);
            pDialog.setMessage("接收任務中...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            // getting updated data from TextView 接資料從執行緒
            String id = args[0];
//                    user_id = args[1],
//                    description = args[2];

            SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.TAIWAN);
            String date = sDateFormat.format(new java.util.Date());

            Log.d("TIME", date);

            //只需上傳 Worker id 、 get_time
            // Building Parameters
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", id));
            params.add(new BasicNameValuePair("worker_id", "1"));
            params.add(new BasicNameValuePair("get_time", date));
            params.add(new BasicNameValuePair("process", "1"));

            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(Url.url_receive_task, "POST", params);

            // check json success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);


                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();

            DatabaseHandler db = new DatabaseHandler(getApplicationContext());

            SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.TAIWAN);
            String date = sDateFormat.format(new java.util.Date());

            TaskFunctions clean = new TaskFunctions();
            clean.cleanTask(getApplicationContext());

            db.addTask(id, //id
                    name,
                    address,
                    age,
                    sex,
                    notice,
                    type,
                    time,
                    date,
                    "1", //process
                    "1", //worker_id
                    service_id,
                    note,
                    major,
                    minor
            );

            Intent n = new Intent(getApplicationContext(), Process.class);
            //            i.putExtra("shopID", String.valueOf(shopID));
            //            i.putExtra("sum", String.valueOf(sum));
            startActivity(n);
            finish();
        }
    }

}
