package com.slivercare.scworker;

/**
 * Created by bryan on 2016/8/16.
 */
public class Url {

    public static String url_get_task = "http://125.227.141.117:16817/get_task.php";

    public static String url_get_task_detail = "http://125.227.141.117:16817/get_task_detail.php";

    public static String url_receive_task = "http://125.227.141.117:16817/receive_task.php";

    public static String url_worker_get_history = "http://125.227.141.117:16817/worker_get_history.php";

    public static String url_worker_start_time = "http://125.227.141.117:16817/worker_start_time.php";

    public static String url_worker_give_up_task = "http://125.227.141.117:16817/worker_give_up_task.php";

    public static String url_worker_check_complete = "http://125.227.141.117:16817/worker_check_complete.php";

}
