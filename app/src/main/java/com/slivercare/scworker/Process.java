package com.slivercare.scworker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by bryan on 2016/8/23.
 */
public class Process extends Activity {

    TextView my_task;

    TextView process_name, process_address, process_age, process_sex, process_notice;
    TextView process_id, process_type, process_time, process_get_time, process_service_id, process_note;

    Button btnGiveUp, btnCheck;

    String toast;

    // Progress Dialog
    private ProgressDialog pDialog;
    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.process);

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        // Hashmap to load data from the Sqlite database
        HashMap<String, String> task = new HashMap<String, String>();
        task = db.getTaskDetails();


        my_task = (TextView) findViewById(R.id.my_task);

        process_name = (TextView) findViewById(R.id.process_name);
        process_address = (TextView) findViewById(R.id.process_address);
        process_age = (TextView) findViewById(R.id.process_age);
        process_sex = (TextView) findViewById(R.id.process_sex);
        process_notice = (TextView) findViewById(R.id.process_notice);

        process_id = (TextView) findViewById(R.id.process_id);
        process_type = (TextView) findViewById(R.id.process_type);
        process_time = (TextView) findViewById(R.id.process_time);
        process_service_id = (TextView) findViewById(R.id.process_service_id);
        process_note = (TextView) findViewById(R.id.process_note);
        process_get_time = (TextView) findViewById(R.id.process_get_time);

        btnGiveUp = (Button) findViewById(R.id.process_give_up);
        btnCheck = (Button) findViewById(R.id.process_check);

        if (task.get("id") == null) {
            SpannableString content = new SpannableString("無進行中任務");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            my_task.setText(content);
        } else {
            //為了加底線
            SpannableString content = new SpannableString("進行中任務");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            my_task.setText(content);
        }


        //從SQLite取得資料
        process_id.setText(task.get("id"));

        process_name.setText(task.get("name"));
        process_address.setText(task.get("address"));
        process_age.setText(task.get("age"));
        process_sex.setText(task.get("sex"));
        process_notice.setText(task.get("notice"));

        process_type.setText(task.get("type"));
        process_time.setText(task.get("time"));
        process_service_id.setText(task.get("service_id"));
        process_note.setText(task.get("note"));
        process_get_time.setText(task.get("get_time"));


        btnGiveUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                // Hashmap to load data from the Sqlite database
                HashMap<String, String> task = new HashMap<String, String>();
                HashMap<String, String> check = new HashMap<String, String>();
                task = db.getTaskDetails();
                check = db.getCheck();


                if (task.get("id") == null) {
                    Context context = getApplication();
                    Toast.makeText(context, "沒有任務可放棄", Toast.LENGTH_LONG).show();

                } else if ((check.get("check_test") != null)) {
                    if (Integer.valueOf(check.get("check_test")) == 1) {
                        Context context = getApplication();
                        Toast.makeText(context, "已到達目的地，無法放棄任務", Toast.LENGTH_LONG).show();
                    }
                } else {
                    new GiveUp().execute();
                }
            }

        });

//Worker 上傳 process = 4
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                // Hashmap to load data from the Sqlite database
                HashMap<String, String> task = new HashMap<String, String>();
                task = db.getTaskDetails();

                if (task.get("id") == null) {
                    Context context = getApplication();
                    Toast.makeText(context, "沒有任務可完成", Toast.LENGTH_LONG).show();
                } else {
                    new CheckComplete().execute();
                }
            }

        });

    }


    class GiveUp extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Process.this);
            pDialog.setMessage("放棄任務中...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            // Hashmap to load data from the Sqlite database
            HashMap<String, String> task = new HashMap<String, String>();
            task = db.getTaskDetails();

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", task.get("id")));

            JSONObject json = jsonParser.makeHttpRequest(Url.url_worker_give_up_task, "POST", params);

            // check json success tag
            try {
                int success = json.getInt("success");
                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();

            Context context = getApplication();
            Toast.makeText(context, "放棄任務", Toast.LENGTH_LONG).show();

            TaskFunctions clean = new TaskFunctions();
            clean.cleanTask(getApplicationContext());

            Intent n = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(n);
            finish();

        }
    }

    //檢查是否完成
    class CheckComplete extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Process.this);
            pDialog.setMessage("確認任務是否完成...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            // Hashmap to load data from the Sqlite database
            HashMap<String, String> task = new HashMap<String, String>();
            task = db.getTaskDetails();

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", task.get("id")));

            JSONObject json = jsonParser.makeHttpRequest(Url.url_worker_check_complete, "POST", params);

            // check json success tag
            try {
                int success = json.getInt("success");
                if (success == 1) {
                    //success
                    toast = "任務順利完成";
                    TaskFunctions clean = new TaskFunctions();
                    clean.cleanTask(getApplicationContext());
                } else if (success == 2) {

                    // failed to update
                    toast = "任務尚未完成";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();

            Context context = getApplication();
            Toast.makeText(context, toast, Toast.LENGTH_LONG).show();

            if (toast == "任務順利完成") {
                Intent n = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(n);
                finish();
            }

        }
    }


}
