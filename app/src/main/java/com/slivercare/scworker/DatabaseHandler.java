package com.slivercare.scworker;

import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.HashMap;

/**
 * Created by bryan on 2016/8/26.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "sc_worker";

    // Login table name
    private static final String TABLE_CHECK = "login";

    // Login Table Columns names
    private static final String KEY_ID = "id";

    private static final String KEY_NAME = "name";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_AGE = "age";
    private static final String KEY_SEX = "sex";
    private static final String KEY_NOTICE = "notice";

    private static final String KEY_TYPE = "type";
    private static final String KEY_TIME = "time";
    private static final String KEY_SERVICE_ID = "service_id";
    private static final String KEY_NOTE = "note";

    private static final String KEY_PROCESS = "process";
    private static final String KEY_WORKER_ID = "worker_id";
    private static final String KEY_GET_TIME = "get_time";

    private static final String KEY_MAJOR = "major";
    private static final String KEY_MINOR = "minor";

    private static final String KEY_CHECK_TABLE = "login_check";
    private static final String KEY_CHECK = "check_test";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_CHECK + "("
                + KEY_ID + " TEXT,"
                + KEY_NAME + " TEXT,"
                + KEY_ADDRESS + " TEXT,"
                + KEY_AGE + " TEXT,"
                + KEY_SEX + " TEXT,"
                + KEY_NOTICE + " TEXT,"
                + KEY_TYPE + " TEXT,"
                + KEY_TIME + " TEXT,"
                + KEY_GET_TIME + " TEXT,"
                + KEY_PROCESS + " TEXT,"
                + KEY_WORKER_ID + " TEXT,"
                + KEY_SERVICE_ID + " TEXT,"
                + KEY_NOTE + " TEXT,"
                + KEY_MAJOR + " TEXT,"
                + KEY_MINOR + " TEXT" + ")";

        String CREATE_CHECK_TABLE = " CREATE TABLE " + KEY_CHECK_TABLE + " ("
                + KEY_CHECK + " TEXT" + ")";

        db.execSQL(CREATE_CHECK_TABLE);

        db.execSQL(CREATE_LOGIN_TABLE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHECK);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addTask(String id ,String name, String address, String age, String sex, String notice, String type, String time, String get_time, String process, String worker_id, String service_id, String note, String major, String minor) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, id);

        values.put(KEY_NAME, name);
        values.put(KEY_ADDRESS, address);
        values.put(KEY_AGE, age);
        values.put(KEY_SEX, sex);
        values.put(KEY_NOTICE, notice);

        values.put(KEY_TYPE, type);
        values.put(KEY_TIME, time);
        values.put(KEY_GET_TIME, get_time);
        values.put(KEY_PROCESS, process);
        values.put(KEY_WORKER_ID, worker_id);
        values.put(KEY_SERVICE_ID, service_id);
        values.put(KEY_NOTE, note);

        values.put(KEY_MAJOR, major);
        values.put(KEY_MINOR, minor);

        // Inserting Row
        db.insert(TABLE_CHECK, null, values);
        db.close(); // Closing database connection
    }

    /**
     * Storing user details in database
     * */
    public void addCheck(String check) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues ch_values = new ContentValues();
        ch_values.put(KEY_CHECK, "1");

        // Inserting Row
        db.insert(KEY_CHECK_TABLE, null, ch_values);
        db.close(); // Closing database connection
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getTaskDetails(){
        HashMap<String,String> task = new HashMap<String,String>();
        String selectQuery = "SELECT  * FROM " + TABLE_CHECK;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            task.put("id", cursor.getString(0));

            task.put("name", cursor.getString(1));
            task.put("address", cursor.getString(2));
            task.put("age", cursor.getString(3));
            task.put("sex", cursor.getString(4));
            task.put("notice", cursor.getString(5));

            task.put("type", cursor.getString(6));
            task.put("time", cursor.getString(7));
            task.put("get_time", cursor.getString(8));
            task.put("process", cursor.getString(9));
            task.put("worker_id", cursor.getString(10));
            task.put("service_id", cursor.getString(11));
            task.put("note", cursor.getString(12));

            task.put("major", cursor.getString(13));
            task.put("minor", cursor.getString(14));
        }
        cursor.close();
        db.close();
        // return user
        return task;
    }



    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getCheck(){
        HashMap<String,String> check = new HashMap<String,String>();
        String selectQuery = "SELECT  * FROM " + KEY_CHECK_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            check.put("check_test", cursor.getString(0));
        }
        cursor.close();
        db.close();
        // return user
        return check;
    }



    /**
     * Getting user login status
     * return true if rows are there in table
     * */
    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CHECK;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();

        // return row count
        return rowCount;
    }


    /**
     * Re crate database
     * Delete all tables and create them again
     * */
    public void resetTables(){
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_CHECK, null, null);
        db.delete(KEY_CHECK_TABLE, null, null);
        db.close();
    }
}//Database
